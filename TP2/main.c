////////////////
/// Includes ///
////////////////
#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>



////////////////
//// Defines ///
////////////////
#define MASTER 0        //ID of master process
#define VECTOR_SIZE 32	//Size of arrays that will be added together.



////////////////
/// DataTypes //
////////////////
//enum {MSG_PING, MSG_ACK};
typedef struct
{
    int to;			//Rank of Destination
    int from;		//Rank from sender
    int tag;		//Tag
    int len;		//Number of elements
    void* msg;		//Pointer to payload
    MPI_Datatype type;	    //Datatype
    MPI_Status status;      //Status tructure
} msg_t;



////////////////
/// fnDeclare //
////////////////
void fn_vector_init(int**, int**, int**);
void fn_master_task();
void fn_slave_task();
void fn_print_vector(int *ptr, int size, char *name);



////////////////
//// Globals ///
////////////////
/* MPI variables */
int g_world_size;   //MPI world size
int g_my_rank;      //Rank of current process
/* Task specifications */
int g_elements_per_proc;
/* Pointers to vectors */
int *g_v1_sub = NULL; //Subsection of v1
int *g_v2_sub = NULL; //Subsection of v2
int *g_v3_sub = NULL; //Subsection of v3





////////////////
///   MAIN   ///
////////////////
int main(int argc, char** argv)
{
    /* Initialization of MPI environment */
    MPI_Init(&argc, &argv);                         // Initialize the MPI environment
    MPI_Comm_size(MPI_COMM_WORLD, &g_world_size);   // Get the number of processes
    MPI_Comm_rank(MPI_COMM_WORLD, &g_my_rank);      // Get the rank of the current process

    /* Calculate number of elements per process */
    g_elements_per_proc = VECTOR_SIZE / g_world_size;
    
    /* Allocate memory and initialize vectors */
    int *v1, *v2, *v3;
    switch(g_my_rank)
    {
    case MASTER:
        // Allocate and Initialize big vectors
        fn_vector_init(&v1, &v2, &v3);
        printf("\n");
    default:
        // Allocate small vectors
        g_v1_sub = (int *) malloc( g_elements_per_proc*sizeof(int) ); //Subsection of v1
        g_v2_sub = (int *) malloc( g_elements_per_proc*sizeof(int) ); //Subsection of v2
        g_v3_sub = (int *) malloc( g_elements_per_proc*sizeof(int) ); //Subsection of v3
        break;
    
    }
    
    
    /* Synchronize processes */
    //MPI_Barrier(MPI_COMM_WORLD);    
     
    //Scatter v1
    MPI_Scatter(
        v1,                     //void* send_data,
        g_elements_per_proc,    //int send_count,
        MPI_INT,                //MPI_Datatype send_datatype,
        g_v1_sub,               //void* recv_data,
        g_elements_per_proc,    //int recv_count,
        MPI_INT,                //MPI_Datatype recv_datatype,
        MASTER,                 //int root,
        MPI_COMM_WORLD);        //MPI_Comm communicator);
        
    //Scatter v2
    MPI_Scatter(
        v2,                     //void* send_data,
        g_elements_per_proc,    //int send_count,
        MPI_INT,                //MPI_Datatype send_datatype,
        g_v2_sub,               //void* recv_data,
        g_elements_per_proc,    //int recv_count,
        MPI_INT,                //MPI_Datatype recv_datatype,
        MASTER,                 //int root,
        MPI_COMM_WORLD);        //MPI_Comm communicator);
    
    
    // Test
    printf("Hola soy %d y mi fragmento es:\n", g_my_rank);
    fn_print_vector(g_v1_sub, g_elements_per_proc, "v1_sub");
    
    // Add v1 and v2
    int i;
    for(i = 0; i < g_elements_per_proc; i++)
        g_v3_sub[i] = g_v1_sub[i] + g_v2_sub[i];
        
    // Gather v3
    MPI_Gather(
        g_v3_sub,                   //void* send_data,
        g_elements_per_proc,        //int send_count,
        MPI_INT,                    //MPI_Datatype send_datatype,
        v3,                         //void* recv_data,
        g_elements_per_proc,        //int recv_count,
        MPI_INT,                    //MPI_Datatype recv_datatype,
        MASTER,                     //int root,
        MPI_COMM_WORLD);            //MPI_Comm communicator)

    
    //Let master print all vectors
    if(MASTER == g_my_rank)
    {
        printf("\n");
        fn_print_vector(v1, VECTOR_SIZE, "v1" );
	    fn_print_vector(v2, VECTOR_SIZE, "v2" );
	    fn_print_vector(v3, VECTOR_SIZE, "v3" );
    }
    
    
    /* Free allocated memory */
    if(MASTER == g_my_rank)
    {
	    free(v1);
	    free(v2);	
	    free(v3);
    }
    
    free(g_v1_sub);
    free(g_v2_sub);
    free(g_v3_sub);

    /* Finalize MPI environment */
    MPI_Finalize();
return 0;
}



////////////////
/// fnDefine ///
////////////////
void fn_vector_init(int **v1, int **v2, int **v3)
{
    /* Counter */
    int i;
    
    /* Allocate memory for v1 and v2 */
	*v1 = (int *) malloc( VECTOR_SIZE*sizeof(int) );
	*v2 = (int *) malloc( VECTOR_SIZE*sizeof(int) );
	*v3 = (int *) malloc( VECTOR_SIZE*sizeof(int) );
	
	/* Initialize vectors */
	for(i = 0; i < VECTOR_SIZE; i++ )
	{
	    (*v1)[i] = i;
	    (*v2)[i] = i*i;
	    (*v3)[i] = 0;
	}
	
	/* Print vectors */
	fn_print_vector(*v1, VECTOR_SIZE, "v1" );
	fn_print_vector(*v2, VECTOR_SIZE, "v2" );
	fn_print_vector(*v3, VECTOR_SIZE, "v3" );
}
void fn_master_task()
{
    
}

void fn_slave_task()
{
    //printf("I'm slave %02d\n", g_my_rank );
}

void fn_print_vector(int *ptr, int size, char *name)
{
    /* Counters */
    int i;

    /* Pretty Print */
    printf("%s = [", name);
	for(i = 0; i < size; i++ ) printf("%3d ", ptr[i]);
	printf("]\n"); 
}
