#include<stdio.h>
#include<mpi.h>

#define MSG_SIZE 8

void task0();
void task1();


int main(int argc, char** argv)
{
	// Initialize the MPI environment
	MPI_Init(&argc, &argv);

	// Get the number of processes
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	
	// Get the rank of the process
	int myRank;
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

	switch(myRank)
	{
	case 0:
		task0();
		break;
	case 1: 
		task1();
		break;
	default:
		break;
	}

	printf("Goodby from process %d\n", myRank);
	MPI_Finalize();
return 0;
}

//Send users input to process 1
void task0()
{
	//Read user's input
	char user_input[MSG_SIZE];
	scanf("%s",user_input);

	//Send message to process 1
	int dest = 1;	//Target process
	int tag = 0;	//message tag
	MPI_Send(user_input, MSG_SIZE, MPI_CHAR, dest, tag, MPI_COMM_WORLD);
}
void task1()
{
	//Read message from process 0
	char msg_payload[MSG_SIZE];
	MPI_Status status;
	MPI_Recv(msg_payload, MSG_SIZE, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

	printf("I received \"%s\" as message\n", msg_payload);
}

