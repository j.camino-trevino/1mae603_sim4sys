#include<stdio.h>
#include<mpi.h>

#define MSG_SIZE 8
enum {MSG_PING, MSG_ACK};

typedef struct
{
	int to;			//Rank of Destination
	int from;		//Rank from sender
	int tag;		//Tag
	int len;		//Number of elements
	void* msg;		//Pointer to payload
	MPI_Datatype type;	//Datatype
	MPI_Status status;
} msg_t;

void task0();
void task1();

int world_size;
int myRank;

int main(int argc, char** argv)
{
	// Initialize the MPI environment
	MPI_Init(&argc, &argv);

	// Get the number of processes
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	
	// Get the rank of the process
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	
	//Execute process-specific tasks
	switch(myRank)
	{
	case 0: task0(); break;
	case 1: task1(); break;
	default:         break;
	}

	printf("Goodbye from process %d\n", myRank);
	MPI_Finalize();
return 0;
}

//Send users input to process 1
void task0()
{
	//Create message structure
	msg_t m = {.len=MSG_SIZE, .type=MPI_CHAR};

	//Update structure to send HOW R U? to process 0
	m.to = 1; m.msg = "HOW R U?"; m.tag = MSG_PING;
	printf("I want to send %s\n", m.msg);
	//Send message to process 1
//	MPI_Send(m.msg, m.len, m.type, m.to, m.tag, MPI_COMM_WORLD);
//	printf("%d: Tx: \"%s\"\n", myRank, m.msg);

	//Update message structure to expect an answer
//	m.from = 1; m.tag = MSG_ACK;

	//Listen for ACK message
//	MPI_Recv(m.msg, m.len, m.type, m.from, m.tag, MPI_COMM_WORLD, &m.status);
//	printf("%d: Rx: \"%s\"\n", myRank, m.msg);
}
void task1()
{
	//Create message structure
	msg_t r = {.len=MSG_SIZE, .type=MPI_CHAR};

	//Update structure expect message from 
        r.from = 0; r.tag = MSG_PING;

	//Read PING message from process 0
//	MPI_Recv(r.msg, r.len, r.type, r.from, r.tag, MPI_COMM_WORLD, &r.status);
//	printf("%d: Rx: \"%s\"\n", myRank, r.msg);	

	//Update structure to send an ACK answer
//	r.to = 0; r.tag = MSG_ACK; r.msg = "FINE TY!";

	//Send message to process 0
//	MPI_Send(r.msg, r.len, r.type, r.to, r.tag, MPI_COMM_WORLD);
//	printf("%d: Tx: \"%s\"\n", myRank, r.msg);
}
