#include<stdio.h>
#include<mpi.h>

int main(int argc, char** argv)
{
	// Initialize the MPI environment
	MPI_Init(&argc, &argv);

	// Get the number of processes
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	
	// Get the rank of the process
	int myRank;
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

	if(2 == myRank) printf("Hello :)\n\n\n");

	printf("Hello, I am task %d/%d\n", myRank, world_size);

	MPI_Finalize();

return 0;
}
