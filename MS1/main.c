//#define PARALLEL_OFF

////////////////
/// Includes ///
////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#ifndef PARALLEL_OFF   
#include <mpi.h>
#endif



////////////////
//// Defines ///
////////////////
#define MASTER_ID 0                 //ID of master process
#define TRUE 1
#define FALSE 0
#define IMAGE_IN "logo.pgm"         //Name of input image
#define IMAGE_OUT "logo2.pgm"       //Name of output image




////////////////
/// fnDeclare //
////////////////
int* read_image (char file_name[], int *p_h, int *p_w, int *p_levels);                      //Read image file
void write_image (int *image, char file_name[], int h, int w, int levels);                  //Write image file
void row_convolution(int *row_in, int *row_out, int nCols, int isFirstRow, int isLastRow);  //Do convolution on a single row
void funny_pattern(int *row_out, int nCols);                                   //Funny pattern for probing purposes
void zeros(int *row_out, int nCols);
void serial_processing(void);
//Stage #0
void fn_S0_masterInit(void);
//Stage #1
void fn_S1_masterInit(void);
void fn_S1_BcastCtrl(void);
void fn_S1_subMalloc(void);
void fn_S1_BcastData(void);
void fn_S1_DoMyShare(void);
void fn_S1_GatherData(void);
void fn_S1_freeMemory(void);
//Stage #2
void fn_S2_masterInit(void);
void fn_S2_BcastCtrl(void);
void fn_S2_subMalloc(void);
void fn_S2_ScatterData(void);
void fn_S2_DoMyShare(void);
void fn_S2_GatherData(void);
void fn_S2_freeMemory(void);
//Stage #3
void fn_S3_masterInit(void);
void fn_S3_BcastCtrl(void);
void fn_S3_subMalloc(void);
void fn_S3_SendData(void);
void fn_S3_DoMyShare(void);
void fn_S3_ReceiveData(void);
void fn_S3_freeMemory(void);


////////////////
/// Constants //
////////////////
#define KERNEL_SIZE 3
const double KERNEL[KERNEL_SIZE][KERNEL_SIZE] = {
                                                    { 0.0, -1.0,  0.0}, 
                                                    {-1.0,  2.5, -1.0}, 
                                                    { 0.0, -1.0,  0.0}
                                                };



////////////////
//// Globals ///
////////////////
// MPI variables 
int g_world_size;   //MPI world size
int g_my_rank;      //Rank of current process
// Stage #0 : Init  ***MASTER ONLY***
int *g_S0_img_in;
int *g_S0_img_out;
int g_S0_img_rows;
int g_S0_img_cols;
int g_S0_img_levels;
// Stage #1 : Modulo
int g_S1_numRows;
int g_S1_numCols;
int g_S1_isNeeded;
int g_S1_hasFirst;
int g_S1_hasLast;
int g_S1_sub_in_numel;
int g_S1_sub_out_numel;
int *g_S1_sub_in;
int *g_S1_sub_out;
// Stage #2 whole division
int g_S2_masterOffset;     //Offset for current working point on img_in or img_out
int g_S2_numRows;
int g_S2_numCols;
int g_S2_sub_in_numel;
int g_S2_sub_out_numel;
int *g_S2_sub_in;
int *g_S2_sub_out;
// Stage #3 : Remanents
int g_S3_masterOffset_in;
int g_S3_masterOffset_out;
int g_S3_numRows;
int g_S3_numCols;
int g_S3_sub_in_numel;
int g_S3_sub_out_numel;
int *g_S3_sub_in;
int *g_S3_sub_out;



////////////////
///   MAIN   ///
////////////////
int main(int argc, char** argv)
{
    /* Initialization of MPI environment */
    #ifndef PARALLEL_OFF
    MPI_Init(&argc, &argv);                         // Initialize the MPI environment
    MPI_Comm_size(MPI_COMM_WORLD, &g_world_size);   // Get the number of processes
    MPI_Comm_rank(MPI_COMM_WORLD, &g_my_rank);      // Get the rank of the current process}
    #endif

    /* Set of variables */
    struct timeval tdeb, tfin;

/* For serial code ONLY */
#ifdef PARALLEL_OFF
    //Read image and copy output, bien sur !
    fn_S0_masterInit();

    //Start timer
    gettimeofday(&tdeb, NULL);

    //Do serial processing
    serial_processing();
#else
/* Stage #0 : Init */
    //Initialize full-scale images
    if(MASTER_ID == g_my_rank) fn_S0_masterInit();
    
    //Wait for all processes
    MPI_Barrier(MPI_COMM_WORLD);

    //Start timer
    gettimeofday(&tdeb, NULL);


/* Stage #1 : Modulo */
    //Make initial calculations
    if(MASTER_ID == g_my_rank) fn_S1_masterInit();
    
    //Broadcast "control" information
    fn_S1_BcastCtrl();
    
    //Continue only if Stage #1 is actually needed
    if(g_S1_isNeeded)
    {
        //Allocate memory for input and output sub
        fn_S1_subMalloc();

        //Broadcast data
        fn_S1_BcastData();

        //Do individual processing
        fn_S1_DoMyShare();

        //Gather data
        fn_S1_GatherData();

        //Free memory
        fn_S1_freeMemory();
    }


/* Stag #2 : Whole division */
    //Initialize variables
    if(MASTER_ID == g_my_rank) fn_S2_masterInit();

    //Broadcast control information
    fn_S2_BcastCtrl();

    //Allocate memory for input and output sub
    fn_S2_subMalloc();

    //Scatter image data
    fn_S2_ScatterData();

    //Process your share
    fn_S2_DoMyShare();

    //Gather data
    fn_S2_GatherData();

    //Free memory
    fn_S2_freeMemory();

/* Stage #3 : Remanents */
    //Initialize variables
    if(MASTER_ID == g_my_rank) fn_S3_masterInit();

    //Broadcast control information
    fn_S3_BcastCtrl();

    //Allocate memory for input and output sub
    fn_S3_subMalloc();

    //Send and receive data ( Point to point communication :( )
    fn_S3_SendData();

    //Process my lines
    fn_S3_DoMyShare();

    //Send processed data back to master
    fn_S3_ReceiveData();

    //Free memory
    fn_S3_freeMemory();
#endif


    //End timer
    gettimeofday(&tfin, NULL);

    //Print execution time in microseconds
    #ifndef PARALLEL_OFF
    if(MASTER_ID == g_my_rank) printf ("computation time (microseconds): %ld\n",  (tfin.tv_sec - tdeb.tv_sec)*1000000 + (tfin.tv_usec - tdeb.tv_usec));
    #else
    printf ("computation time (microseconds): %ld\n",  (tfin.tv_sec - tdeb.tv_sec)*1000000 + (tfin.tv_usec - tdeb.tv_usec));
    #endif

    /*
    if(MASTER_ID == g_my_rank)
    {
        
        // image processing (just a copy in this example)
        gettimeofday(&tdeb, NULL);
        
        gettimeofday(&tfin, NULL);
        printf ("computation time (microseconds): %ld\n",  (tfin.tv_sec - tdeb.tv_sec)*1000000 + (tfin.tv_usec - tdeb.tv_usec));

    }
    */

    #ifndef PARALLEL_OFF
    /* Write img_out to file */
    if(MASTER_ID == g_my_rank) write_image (g_S0_img_out, IMAGE_OUT, g_S0_img_rows, g_S0_img_cols, g_S0_img_levels);

    /* Free full image pointers */
    if(MASTER_ID == g_my_rank)
    {
        free (g_S0_img_in);
        free (g_S0_img_out);
    }
    #else
    /* Write image to file */
    write_image (g_S0_img_out, IMAGE_OUT, g_S0_img_rows, g_S0_img_cols, g_S0_img_levels);

    /* Free pointers */
    free (g_S0_img_in);
    free (g_S0_img_out);
    #endif

    /* Finalize MPI environment */
    #ifndef PARALLEL_OFF
    MPI_Finalize();
    #endif
return 0;
}



////////////////
/// fnDefine ///
////////////////
/*
* read an image from a file  and dynamic memory allocation for it
* param1 : name of the image file
* param2 : where to store the height
* param3 : where to store the weight
* param4 : where to store the number of levels
* return : address of the pixels
*/
int * read_image (char file_name[], int *p_h, int *p_w, int *p_levels){
  FILE *fin;
  int i, j, h, w, levels ;
  char buffer[80];
  int *image;

  /* open P2 image */
  fin = fopen (IMAGE_IN, "r");
  if (fin == NULL){
    printf ("file open error\n");
    exit(-1);
  } 
  fscanf (fin, "%s", buffer);
  if (strcmp (buffer, "P2")){
    printf ("the image format must be P2\n");
    exit(-1);
  }
  fgets (buffer, 80, fin);
  fgets (buffer, 80, fin);
  fscanf (fin, "%d%d", &w, &h);
  fscanf (fin, "%d", &levels);
  printf ("image reading ... h = %d w = %d\n", h, w);
  
  /* dynamic memory allocation for the pixels */
  image = malloc (h*w*sizeof(int));

  /* pixels reading */
  for (i = 0; i < h ; i++)
    for (j = 0; j < w; j++)
       fscanf (fin, "%d", image +i*w +j); 
  fclose (fin);

  *p_h = h;
  *p_w = w;
  *p_levels=levels;
  return image;
}
/*
* write an image in a file
* param1 : address of the pixels
* param2 : name of the image file
* param3 : the height
* param4 : the weight
* param5 : the number of levels
* return : void
*/
void write_image (int *image, char file_name[], int h, int w, int levels){
  FILE *fout;
  int i, j;

  /* open the file */
  fout=fopen(IMAGE_OUT,"w");
  if (fout == NULL){
    printf ("file opening error\n");
    exit(-1);
  }
  
  /* header write */
  fprintf(fout,"P2\n# test \n%d %d\n%d\n", w, h, levels);
  /* format P2, commentary, w x h points, levels */

  /* pixels writing*/
  for (i = 0; i < h ; i++)
    for (j = 0; j < w; j++)
       fprintf (fout, "%d\n", image[i*w+j]); 

  fclose (fout);
  return;
}

void row_convolution(int *row_in, int *row_out, int nCols, int isFirstRow, int isLastRow)
{
    /* Accumulator */
    double acc;
    int cell;

    /* References */
    int * row_in_above = row_in - nCols;
    int * row_in_below = row_in + nCols;

    /* Iterator variables */
    int i;

    /* Iterate on row */
    for(i = 0; i < nCols; i++)
    {

        //Reset accumulator
        acc = 0.0;

        // Check row above
        if(0 == isFirstRow)
        {
            //Left
            if(0 == i) 
                acc += ( (double) (row_in_above[i-1]) ) * KERNEL[2][2];

            //Middle
            acc += ( (double) (row_in_above[i]) ) * KERNEL[2][1];

            //Right
            if((nCols-1) == i) 
                acc += ( (double) (row_in_above[i+1]) ) * KERNEL[2][0];
        }

        //Center row
            //Left
            if(0 == i) 
                acc += ( (double) (row_in[i-1]) ) * KERNEL[1][2];

            //Middle
            acc += ( (double) (row_in[i]) ) * KERNEL[1][1];

            //Right
            if((nCols-1) == i) 
                acc += ( (double) (row_in[i+1]) ) * KERNEL[1][0];

        // Check row below
        if(0 == isLastRow)
        {
            //Left
            if(0 == i) 
                acc += ( (double) (row_in_below[i-1]) ) * KERNEL[0][2];

            //Middle
            acc += ( (double) (row_in_below[i]) ) * KERNEL[0][1];

            //Right
            if((nCols-1) == i) 
                acc += ( (double) (row_in_below[i+1]) ) * KERNEL[0][0];
        }

        //Cast
        cell = ((int) (acc));

        //Cast corrections
        if(cell <   0) cell = 0;
        if(cell > 255) cell = 255;

        /* Asign cell to output row */
        row_out[i] = cell;
    }

}

void funny_pattern(int *row_out, int nCols)
{
    int i = 0;
    for(i = 1; i < nCols; i += 2)
    {
        row_out[i-1] = 0;
        row_out[i] = 255;
    }
}

void zeros(int *row_out, int nCols)
{
    int i = 0;
    for(i = 0; i < nCols; i++)
    {
        row_out[i] = 0;
    }
}

void serial_processing(void)
{
    //Counter
    int i;

    //Flags
    int isFirst;
    int isLast;

    //Pointers to row
    int *row_in;
    int *row_out;

    //Iterate over the entire array
    for(i = 0; i < g_S0_img_rows; i++)
    {
        //Update flags
        isFirst = (0 == i);
        isLast = ( (g_S0_img_rows-1) == i );

        //Update ptr for input row
        row_in = g_S0_img_in + (i * g_S0_img_cols);
        //Update ptr for output row
        row_out = g_S0_img_out + (i * g_S0_img_cols);

        //Do convolution
        row_convolution(row_in, row_out, g_S0_img_cols, isFirst, isLast);
    }
    
}

//Stage #0
void fn_S0_masterInit(void)
{
    //Read input image
    g_S0_img_in  = read_image(IMAGE_IN, &g_S0_img_rows, &g_S0_img_cols, &g_S0_img_levels); 
    
    //Allocate memory for output image
    g_S0_img_out = malloc(g_S0_img_rows * g_S0_img_cols * sizeof(int));

    //Copy image in on image out
    int i, j;
    for (i = 0; i < g_S0_img_rows ; i++)
    {
        for (j = 0; j < g_S0_img_cols; j++)
            g_S0_img_out[i*g_S0_img_cols+j] = g_S0_img_in[i*g_S0_img_cols+j]; 
    }

}

#ifndef PARALLEL_OFF
//Stage #1
void fn_S1_masterInit(void)
{
    // Calculate the number of rows of Stage #1
    g_S1_numRows = g_S0_img_rows % g_world_size;
    // Calculate the number of cols of Stage #1
    g_S1_numCols = g_S0_img_cols;
    // Define if Stage #1 is actually necessary
    g_S1_isNeeded = (0 == g_S1_numRows) ? (0) : (1);
    // Define if Stage #1 has the first row
    g_S1_hasFirst = 1;
    // Define if Stage #2 has the last row
    g_S1_hasLast = 0;
    //Sub_in takes 1 extra row to allow the last process to do a clean convolution
    g_S1_sub_in_numel = (g_S1_numRows + 1) * g_S1_numCols;
    //Sub_out is a single row
    g_S1_sub_out_numel = (1) * g_S1_numCols;
    // Define the pointer to the input starting position
    g_S1_sub_in = g_S0_img_in;
    // Initialize NULL pointer to output starting position
    g_S1_sub_out = NULL;
}

void fn_S1_BcastCtrl(void)
{
    // Broadcast the number of rows of Stage #1
    MPI_Bcast(
        &g_S1_numRows,      //void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
    // Broadcast the number of cols of full image
    MPI_Bcast(
        &g_S1_numCols,      //void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
    // Broadcast if Stage #1 is actually needed
    MPI_Bcast(
        &g_S1_isNeeded,     //void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
    // Broadcast if Stage #1 has first row
    MPI_Bcast(
        &g_S1_hasFirst,     //void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
    // Broadcast if Stage #1 has last row
    MPI_Bcast(
        &g_S1_hasLast,      //void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
    //Broadcast numel of sub_in in Stage #1
    MPI_Bcast(
        &g_S1_sub_in_numel, //void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
    //Broadcast numel of sub_out in Stage #1
    MPI_Bcast(
        &g_S1_sub_out_numel,//void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
}

void fn_S1_subMalloc(void)
{
    //Master does not need to allocate memory for input sub, as it is already on the full image
    if(MASTER_ID != g_my_rank)
    {
        g_S1_sub_in = malloc(g_S1_sub_in_numel * sizeof(int));
    }

    //Everybody needs to allocate memory for the output sub
    g_S1_sub_out = malloc(g_S1_sub_out_numel * sizeof(int));
}

void fn_S1_BcastData(void)
{
    // Broadcast the number of rows of Stage #1
    MPI_Bcast(
        g_S1_sub_in,        //void* data,
        g_S1_sub_in_numel,  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
}

void fn_S1_DoMyShare(void)
{
    //Build pointer to your respective row
    int *my_row = g_S1_sub_in + (g_my_rank*g_S1_numCols);

    //If you are required to process, do the convolution of your row
    if(g_my_rank < g_S1_numRows)
    {
        row_convolution(
            my_row,                     // int *row_in,
            g_S1_sub_out,               // int *row_out,
            g_S1_numCols,               // int nCols, 
            (MASTER_ID == g_my_rank),   // int isFirstRow, 
            0);                         // int isLastRow);
    }
    //Otherwise, fill with pattern just for probing purposes
    else
    {
        funny_pattern(g_S1_sub_out, g_S1_numCols);   
    }
    
    //Wait for all processes
    MPI_Barrier(MPI_COMM_WORLD);
}

void fn_S1_GatherData(void)
{
    //If some processors didn't do their convolution it doesn't matter
    //Later on the image_out will be properly overwritten in that part
    MPI_Gather(
        g_S1_sub_out,               // void* send_data,
        g_S1_sub_out_numel,         // int send_count,
        MPI_INT,                    // MPI_Datatype send_datatype,
        g_S0_img_out,               // void* recv_data,
        g_S1_sub_out_numel,         // int recv_count,
        MPI_INT,                    // MPI_Datatype recv_datatype,
        MASTER_ID,                  // int root,
        MPI_COMM_WORLD);            // MPI_Comm communicator)

}

void fn_S1_freeMemory(void)
{
    //Master does not free sub_in because it points to full image
    if(MASTER_ID != g_my_rank)
        free(g_S1_sub_in);

    //Everybody frees sub_out
    free(g_S1_sub_out);
}


//Stage #2
void fn_S2_masterInit(void)
{
    //MASTER ONLY : Define the offset on the full image
    g_S2_masterOffset = g_S1_numRows * g_S1_numCols;
    //Define the number of rows per block
    g_S2_numRows = (g_S0_img_rows - g_S1_numRows) / g_world_size;
    //Define the number of cols per block
    g_S2_numCols = g_S0_img_cols;
    //Sub_in is just rows * cols
    g_S2_sub_in_numel = g_S2_numRows * g_S2_numCols;
    //Sub_out is the same size as sub_in
    g_S2_sub_out_numel = g_S2_sub_in_numel;
    //Point to NULL (memory to be allocated here)
    g_S2_sub_in = NULL;
    //Point to NULL (memory to be allocated here)
    g_S2_sub_out = NULL;
}

void fn_S2_BcastCtrl(void)
{
    // Broadcast the number of rows of Stage #2
    MPI_Bcast(
        &g_S2_numRows,      //void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
    // Broadcast the number of cols of Stage #2
    MPI_Bcast(
        &g_S2_numCols,      //void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
    //Broadcast the number of elements of sub_in
    MPI_Bcast(
        &g_S2_sub_in_numel, //void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
    //Broadcast the number of elements of sub_out
    MPI_Bcast(
        &g_S2_sub_out_numel,//void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
}

void fn_S2_subMalloc(void)
{
    //Everybody needs to allocate memory for sub_in, since scatter will treat all processors equal
    g_S2_sub_in = malloc(g_S2_sub_in_numel * sizeof(int));

    //Everybody needs to allocate memory for the output sub
    g_S2_sub_out = malloc(g_S2_sub_out_numel * sizeof(int));
}

void fn_S2_ScatterData(void)
{
    //Divide remaining image on equal parts and istribute them to processors
    MPI_Scatter(
        g_S0_img_in + g_S2_masterOffset,   // void* send_data,
        g_S2_sub_in_numel,  // int send_count,
        MPI_INT,            // MPI_Datatype send_datatype,
        g_S2_sub_in,        // void* recv_data,
        g_S2_sub_in_numel,  // int recv_count,
        MPI_INT,            // MPI_Datatype recv_datatype,
        MASTER_ID,          // int root,
        MPI_COMM_WORLD);    // MPI_Comm communicator)

}

void fn_S2_DoMyShare(void)
{
    //Pointers to input and output rows
    int *row_in;
    int *row_out;
    //flags for convolution
    int isFirst;
    int isLast;
    //"Constant" tags for algorithm
    int FIRST_ROW = 0;
    int LAST_ROW = g_S2_numRows - 1;
    int LAST_PROC = g_world_size -1;
    //Counter
    int i;

    //Iterate over all rows in my block
    for(i = 0; i < g_S2_numRows; i++)
    {
        //Place pointers to input and output rows
        row_in = g_S2_sub_in + (i * g_S2_numCols);
        row_out = g_S2_sub_out + (i * g_S2_numCols);

        //Set default value for flags
        isFirst = FALSE;
        isLast  = FALSE;

        //Very rarely set isFirst flag
        if(MASTER_ID == g_my_rank)
        {
            //Check for first row
            if(FIRST_ROW == i)
            {
                //Set is first only if no S1 was needed
                isFirst = (g_S1_isNeeded) ? (FALSE) : (TRUE);
            }
        }

        //Rarely set isLast flag
        if(LAST_PROC == g_my_rank)
        {
            if(LAST_ROW == i) isLast = TRUE;
        }
        
        //Usually do funny pattern on first row
        if((FIRST_ROW == i) && (FALSE == isFirst)) 
        {
            funny_pattern(row_out, g_S2_numCols);
            continue;
        }

        //Usually do funny pattern on last row
        if((LAST_ROW == i) && (FALSE == isLast))
        {
            funny_pattern(row_out, g_S2_numCols);
            continue;
        }

        //Do proper convolution
        row_convolution(
            row_in,                     // int *row_in,
            row_out,                    // int *row_out,
            g_S2_numCols,               // int nCols, 
            isFirst,                    // int isFirstRow, 
            isLast);                    // int isLastRow);
    }
    
}

void fn_S2_GatherData(void)
{
    MPI_Gather(
        g_S2_sub_out,               // void* send_data,
        g_S2_sub_out_numel,         // int send_count,
        MPI_INT,                    // MPI_Datatype send_datatype,
        g_S0_img_out + g_S2_masterOffset,               // void* recv_data,
        g_S2_sub_out_numel,         // int recv_count,
        MPI_INT,                    // MPI_Datatype recv_datatype,
        MASTER_ID,                  // int root,
        MPI_COMM_WORLD);            // MPI_Comm communicator)
}

void fn_S2_freeMemory(void)
{
    free(g_S2_sub_in);
    free(g_S2_sub_out);
}


//Stage #3
void fn_S3_masterInit(void)
{
    //Set offset on input image, 2 rows before the Start of section 2
    g_S3_masterOffset_in = g_S2_masterOffset - (2*g_S2_numCols);
    //Set offset on output image, 1 row before the start of section 2
    g_S3_masterOffset_out = g_S2_masterOffset - (1*g_S2_numCols);
    //The number of rows is fixed to 2
    g_S3_numRows = 2;
    //The number of cols does not change
    g_S3_numCols = g_S2_numCols;
    //The input is actually 4 rows
    g_S3_sub_in_numel = (2 + g_S3_numRows) * g_S3_numCols;
    //The output is just 2 rows
    g_S3_sub_out_numel = g_S3_numRows * g_S3_numCols;
    //Pointer to sub_in for P0
    g_S3_sub_in = g_S0_img_in + g_S3_masterOffset_in;
    //Pointer to sub_out for P0
    g_S3_sub_out = g_S0_img_out + g_S3_masterOffset_out;
}

void fn_S3_BcastCtrl(void)
{
    // Broadcast the number of rows of Stage #2
    MPI_Bcast(
        &g_S3_numRows,      //void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
    // Broadcast the number of cols of Stage #2
    MPI_Bcast(
        &g_S3_numCols,      //void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
    //Broadcast the number of elements of sub_in
    MPI_Bcast(
        &g_S3_sub_in_numel, //void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
    //Broadcast the number of elements of sub_out
    MPI_Bcast(
        &g_S3_sub_out_numel,//void* data,
        1,                  //int count,
        MPI_INT,            //MPI_Datatype datatype,
        MASTER_ID,          //int root,
        MPI_COMM_WORLD);    //MPI_Comm communicator)
}

void fn_S3_subMalloc(void)
{
    //Master does not need to allocate memory  because data will be transmitted point-to-point
    if(MASTER_ID != g_my_rank)
    {
        //Memory for sub_in
        g_S3_sub_in = malloc(g_S3_sub_in_numel * sizeof(int));
        //Memory for sub_out
        g_S3_sub_out = malloc(g_S3_sub_out_numel * sizeof(int));
    }
}

void fn_S3_SendData(void)
{
    //Iterator
    int i;

    //Pointer to sub_in
    int *sub_in_ptr = g_S0_img_in + g_S3_masterOffset_in ;

    //Master sends the data
    if(MASTER_ID == g_my_rank)
    {
        //Send each process its share
        for(i = 1; i < g_world_size; i++)        
        {
            //Increment pointer
            sub_in_ptr += g_S2_sub_in_numel;

            MPI_Send(
                sub_in_ptr,             // void* data,
                g_S3_sub_in_numel,      // int count,
                MPI_INT,                // MPI_Datatype datatype,
                i,                      // int destination,
                0,                      // int tag,
                MPI_COMM_WORLD);        // MPI_Comm communicator)
        }
    }
    else
    {
        MPI_Recv(
            g_S3_sub_in,                // void* data,
            g_S3_sub_in_numel,          // int count,
            MPI_INT,                    // MPI_Datatype datatype,
            MASTER_ID,                  // int source,
            0,                          // int tag,
            MPI_COMM_WORLD,             // MPI_Comm communicator,
            MPI_STATUS_IGNORE);         // MPI_Status* status)
    }

}

void fn_S3_DoMyShare(void)
{
    //Iterator
    int i;
    //Pointers to row
    int *row_in;
    int *row_out;

    //Everybody skips 1 row on the input
    row_in = g_S3_sub_in + (1) * g_S3_numCols;

    //row_out is initialized at the begining of sub_out
    row_out = g_S3_sub_out;

    //Process two rows
    for(i = 0; i < g_S3_numRows; i++)
    {
        //Do row convolution
        row_convolution(
            row_in,         // int *row_in, 
            row_out,        // int *row_out, 
            g_S3_numCols,   // int nCols, 
            FALSE,          // int isFirstRow, 
            FALSE);         // int isLastRow);

        //Increment pointers
        row_in  += g_S3_numCols;
        row_out += g_S3_numCols;
    }
    
}

void fn_S3_ReceiveData(void)
{
    //Iterator
    int i;

    //Pointer to sub_out
    int *sub_out_ptr = g_S0_img_out + g_S3_masterOffset_out;

    //Wait for all processes
    MPI_Barrier(MPI_COMM_WORLD);

    //Master receives
    if(MASTER_ID == g_my_rank)
    {
        //Receive processed data from each process
        for(i = 1; i < g_world_size; i++)        
        {
            //Increment pointer
            sub_out_ptr += g_S2_sub_out_numel;

            MPI_Recv(
                sub_out_ptr,                // void* data,
                g_S3_sub_out_numel,         // int count,
                MPI_INT,                    // MPI_Datatype datatype,
                i,                          // int source,
                0,                          // int tag,
                MPI_COMM_WORLD,             // MPI_Comm communicator,
                MPI_STATUS_IGNORE);         // MPI_Status* status)

        }
    }
    //Slaves send
    else
    {
        MPI_Send(
            g_S3_sub_out,           // void* data,
            g_S3_sub_out_numel,     // int count,
            MPI_INT,                // MPI_Datatype datatype,
            MASTER_ID,              // int destination,
            0,                      // int tag,
            MPI_COMM_WORLD);        // MPI_Comm communicator)

    }
    
    
}

void fn_S3_freeMemory(void)
{
    //Master does not need to allocate memory  because data will be transmitted point-to-point
    if(MASTER_ID != g_my_rank)
    {
        //Memory for sub_in
        free(g_S3_sub_in);
        //Memory for sub_out
        free(g_S3_sub_out);
    }
}

#endif